package com.comfort.pmtmock.Services;

import com.comfort.pmtmock.Model.Request.CityRequest;
import com.comfort.pmtmock.Model.Request.StateRequest;
import com.comfort.pmtmock.Model.Response.Createuser;
import com.comfort.pmtmock.Model.Response.Otp;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface API {

    @FormUrlEncoded
    @POST("Student_Registration_api_mobile")
    Call<Createuser> createuser(
            @Field("username") String userfull,
            @Field("mobile_number") String umobile,
            @Field("referel_code") String refrealcode,
            @Field("email") String uemail,
            @Field("device_id") String deviceid,
            @Field("device_type") String devicetype,
            @Field("device_token") String devicetoken,
            @Field("address") String address,
            @Field("gender") String gender,
            @Field("stateid") String stateid,
            @Field("cityid") String cityid
    );



    @GET("fetch_State_api_mobile")
    Call<StateRequest> state_response(

    );
    @FormUrlEncoded
    @POST("fetching_city_acc_api_mobile")
    Call<CityRequest> city_response(
            @Field("stateid") String stateid

    );

    @FormUrlEncoded
    @POST("otp_verification")
    Call<Otp> otp_verification(
            @Field("user_id") String userid,
            @Field("otp") String Otp,
            @Field("device_type") String divice,
            @Field("device_id") String diviceid
    );
    //
    @FormUrlEncoded
    @POST("otp_generation")
    Call<Otp> resend_otp(
            @Field("user_id") String userid

    );


}
