package com.comfort.pmtmock.Services;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class Preferences {
    private final static String preferencesName = "PMTMock";//swapnil
    public static Context appContext;


    public static void setUserId(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("setUserId", val);
        editor.commit();
    }
    public static String getUserId() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("setUserId", null);
        return value;
    }

    public static void setLoginStatus(boolean value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putBoolean("loginStatus",value);
        //editor.putString("getCurrentUserId", value);
        editor.commit();
    }
    public static boolean getLoginStatus() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        boolean value = prefs.getBoolean("loginStatus", false);
        return value;
    }

    public static void setServiceId(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("service_id", val);
        editor.commit();
    }
    public static String getServiceId() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("service_id", null);
        return value;
    }
    public static void setEventId(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("event_id", val);
        editor.commit();
    }
    public static String getEventId() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("event_id", null);
        return value;
    }
    public static void setProductId(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("product_id", val);
        editor.commit();
    }
    public static String getProductId() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("product_id", null);
        return value;
    }

    public static void setSubjectId(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("subject_id", val);
        editor.commit();
    }
    public static String getSubjectId() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("subject_id", null);
        return value;
    }

    public static void setUsername(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("Username", val);
        editor.commit();
    }
    public static String getUsername() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("Username", null);
        return value;
    }
    public static void setSubjectName(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("subject_name", val);
        editor.commit();
    }
    public static String getSubjectName() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("subject_name", null);
        return value;
    }

}
