package com.comfort.pmtmock.Model.Request;

import com.comfort.pmtmock.Model.Response.CityResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CityRequest {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private CityResponse[] cityResponses = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CityResponse[] getCityResponses() {
        return cityResponses;
    }

    public void setCityResponses(CityResponse[] cityResponses) {
        this.cityResponses = cityResponses;
    }
}
