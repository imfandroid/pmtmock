package com.comfort.pmtmock.Model.Request;

import com.comfort.pmtmock.Model.Response.StateResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StateRequest {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private StateResponse[] data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public StateResponse[] getData() {
        return data;
    }

    public void setData(StateResponse[] data) {
        this.data = data;
    }
}
