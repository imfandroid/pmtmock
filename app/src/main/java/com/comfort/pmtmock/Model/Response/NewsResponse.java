package com.comfort.pmtmock.Model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewsResponse {

    @SerializedName("news_id")
    @Expose
    private String newsId;
    @SerializedName("state_id")
    @Expose
    private String stateId;
    @SerializedName("livedate")
    @Expose
    private String livedate;
    @SerializedName("imagepath")
    @Expose
    private String imagepath;

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getLivedate() {
        return livedate;
    }

    public void setLivedate(String livedate) {
        this.livedate = livedate;
    }

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }
}
