package com.comfort.pmtmock.Model.Request;

import com.comfort.pmtmock.Model.Response.CurrentAffairResponse;
import com.comfort.pmtmock.Model.Response.ExamCatagoryResponse;
import com.comfort.pmtmock.Model.Response.JobResponse;
import com.comfort.pmtmock.Model.Response.NewsResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeRequest {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("examcategoryData")
    @Expose
    private ExamCatagoryResponse[] examcategoryData = null;
    @SerializedName("jobsData")
    @Expose
    private JobResponse[] jobsData = null;
    @SerializedName("newsData")
    @Expose
    private NewsResponse[] newsData = null;
    @SerializedName("currentAffairsData")
    @Expose
    private CurrentAffairResponse[] currentAffairsData = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ExamCatagoryResponse[] getExamcategoryData() {
        return examcategoryData;
    }

    public void setExamcategoryData(ExamCatagoryResponse[] examcategoryData) {
        this.examcategoryData = examcategoryData;
    }

    public JobResponse[] getJobsData() {
        return jobsData;
    }

    public void setJobsData(JobResponse[] jobsData) {
        this.jobsData = jobsData;
    }

    public NewsResponse[] getNewsData() {
        return newsData;
    }

    public void setNewsData(NewsResponse[] newsData) {
        this.newsData = newsData;
    }

    public CurrentAffairResponse[] getCurrentAffairsData() {
        return currentAffairsData;
    }

    public void setCurrentAffairsData(CurrentAffairResponse[] currentAffairsData) {
        this.currentAffairsData = currentAffairsData;
    }
}