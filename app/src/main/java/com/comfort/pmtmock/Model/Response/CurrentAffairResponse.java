package com.comfort.pmtmock.Model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrentAffairResponse {
    @SerializedName("currentAffairs_id")
    @Expose
    private String currentAffairsId;
    @SerializedName("headline")
    @Expose
    private String headline;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("imagepath")
    @Expose
    private String imagepath;

    public String getCurrentAffairsId() {
        return currentAffairsId;
    }

    public void setCurrentAffairsId(String currentAffairsId) {
        this.currentAffairsId = currentAffairsId;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }
}
