package com.comfort.pmtmock.Model.Response;

public class Otp {
    private boolean status;
    private String message;

    public Otp(boolean status, String message) {
        this.status = status;
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
