package com.comfort.pmtmock.Model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobResponse {

    @SerializedName("job_id")
    @Expose
    private String jobId;
    @SerializedName("state_id")
    @Expose
    private String stateId;
    @SerializedName("imagepath")
    @Expose
    private String imagepath;

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }


}


