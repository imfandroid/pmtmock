package com.comfort.pmtmock.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.comfort.pmtmock.Activity.launchscreen.Login;
import com.comfort.pmtmock.R;

public class CustomPagerAdapter extends PagerAdapter {
    Context mContext;
    LayoutInflater mLayoutInflater;
    public int[] lst_images = {
            R.drawable.img_competitive,
            R.drawable.img_job_notification,
            R.drawable.img_job_news_events,
            R.drawable.img_live_test
    };
    public int[] sst_images = {
            R.drawable.bg_onboarding_left, R.drawable.bg_onboarding_right,
            R.drawable.bg_onboarding_left, R.drawable.bg_onboarding_right
    };
    public String[] imagTitle = {
            "Online Video Course",
            "Online Mock Test",
            "Online Videos",
            "Any Where and any time"
    };
    public String[] botTitle = {
            "Study any Topic, Anytime, Anywhere\n" +
                    "Courses available from Rs. 99 Each",
            "Test your Knowledge by solving \n1000+ Questions\n" +
                    "Be Exam Ready",
            "100+ Online Courses\n" +
                    "Schedule your Study",
            "Use LIVE TEST Here\n" +
                    "Learn, Earn & Repeat\n" +
                    "Earn while Leaning by giving LIVE Test"
    };

    public CustomPagerAdapter(Context mContext) {
        this.mContext = mContext;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return lst_images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        View itemView = mLayoutInflater.inflate(R.layout.singleitem_slider, container, false);

        ImageView imgslide = (ImageView) itemView.findViewById(R.id.slideimg);
        TextView txtTitle = itemView.findViewById(R.id.txt_title);
        TextView txtbottom = itemView.findViewById(R.id.bottom_start);
        TextView txtSkip = itemView.findViewById(R.id.txt_skip);

        imgslide.setImageResource(lst_images[position]);
        txtTitle.setText(imagTitle[position]);
        txtbottom.setText(botTitle[position]);

        container.addView(itemView);
        if (position == 3) {
            txtSkip.setText("Next");
        }
        if (position == 4) {

            mContext.startActivity(new Intent(mContext, Login.class));

        }
        txtSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, Login.class));
            }
        });
        return itemView;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //super.destroyItem(container, position, object);
        container.removeView((LinearLayout) object);

    }
}