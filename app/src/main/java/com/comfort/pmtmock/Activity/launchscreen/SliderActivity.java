package com.comfort.pmtmock.Activity.launchscreen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.androidpagecontrol.PageControl;
import com.comfort.pmtmock.Adapter.CustomPagerAdapter;
import com.comfort.pmtmock.R;
import com.comfort.pmtmock.Services.ZoomOutPageTransformer;

public class SliderActivity extends AppCompatActivity {

    Context mContext;
    private ViewPager viewPager;
    private CustomPagerAdapter myadapter;

    PageControl pageControl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_slider);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.mContext = SliderActivity.this;
        pageControl = findViewById(R.id.page_control);
        viewPager = findViewById(R.id.pager);
        myadapter = new CustomPagerAdapter(SliderActivity.this);
        viewPager.setAdapter(myadapter);
        pageControl.setViewPager(viewPager);
        pageControl.setPosition(0);

        viewPager.setPageTransformer(true, (ViewPager.PageTransformer) new ZoomOutPageTransformer());
    }
}
