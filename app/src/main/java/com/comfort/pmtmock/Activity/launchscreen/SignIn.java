package com.comfort.pmtmock.Activity.launchscreen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.comfort.pmtmock.Model.Request.CityRequest;
import com.comfort.pmtmock.Model.Request.StateRequest;
import com.comfort.pmtmock.Model.Response.CityResponse;
import com.comfort.pmtmock.Model.Response.Createuser;
import com.comfort.pmtmock.Model.Response.StateResponse;
import com.comfort.pmtmock.R;
import com.comfort.pmtmock.Services.Preferences;
import com.comfort.pmtmock.Services.RetrofitClient;
import com.google.android.material.textfield.TextInputEditText;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignIn extends AppCompatActivity {

    Context context;
    ImageButton back;

    TextInputEditText txtname, txtemail, txtmobile, txtaddress, txtrefer;
    MaterialBetterSpinner txtgender, txtcity, txtstate;
    String[] GenderLIST = {"Male", "Female"};
    ArrayList<StateResponse> stateResponses;
    ArrayList<CityResponse> cityResponses;
    ArrayList<String> State = new ArrayList<String>();
    ArrayList<String> City;
    String stateid, cityid, genderid;
    Button btncreateuser;
    String strname, stremail, strmoble, straddress, strrefer;
    String device_type = "Android", device_token = "swa";
    ArrayAdapter<String> city;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        init();
    }


    void init() {

        back=(ImageButton) findViewById(R.id.back);
        backbutton();

        context = SignIn.this;
        txtname = findViewById(R.id.edt_full_name);
        txtemail = findViewById(R.id.edt_email);
        txtmobile = findViewById(R.id.edt_mobile_number);
        txtaddress = findViewById(R.id.edt_address);
        txtrefer = findViewById(R.id.edt_referal_code);

        txtgender = findViewById(R.id.spinn_gender);
        txtcity = findViewById(R.id.spinn_city);
        txtstate = findViewById(R.id.spinn_maha);
        btncreateuser = findViewById(R.id.btn_create_account);
        btncreateuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createuser();
            }
        });
        getstateList();
        City = new ArrayList<String>();
        stateResponses = new ArrayList<>();
        cityResponses = new ArrayList<>();

        final ArrayAdapter<String> gender = new ArrayAdapter<String>(context,
                R.layout.custum_single_spinner, GenderLIST);
        txtgender = (MaterialBetterSpinner)
                findViewById(R.id.spinn_gender);
        txtgender.setAdapter(gender);
        txtgender.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                stateid = stateResponses.get(position).getId();
                int ddfd = position;
                if (ddfd == 0) {
                    genderid = "Male";

                } else {
                    genderid = "Female";

                }
//                getCityList();

            }
        });

        city = new ArrayAdapter<String>(context,
                R.layout.custum_single_spinner, City);
        txtcity = (MaterialBetterSpinner)
                findViewById(R.id.spinn_city);
        txtcity.setAdapter(city);



        ArrayAdapter<String> state = new ArrayAdapter<String>(context,
                R.layout.custum_single_spinner, State);
        txtstate = (MaterialBetterSpinner)
                findViewById(R.id.spinn_maha);
        txtstate.setAdapter(state);
        txtstate.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                stateid = stateResponses.get(position).getId();
                int ddfd = position;

                getCityList();
//                if(stateid.equals(stateResponses.)){
//                    Toast.makeText(SignIn.this, "state id mach", Toast.LENGTH_LONG).show();
//
//                }


            }
        });
        txtcity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                cityid = cityResponses.get(position).getCityId();
                int ddfd = position;
                getCityList();

                txtcity.setAdapter(null);
            }
        });
    }



    void backbutton(){

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //super.onBackPressed();
                onBackPressed();
            }
        });
    }


    void getstateList() {


        Call<StateRequest> call = RetrofitClient.getInstance().getapi()
                .state_response();
        call.enqueue(new Callback<StateRequest>() {
            @Override
            public void onResponse(Call<StateRequest> call, Response<StateRequest> response) {

                StateRequest stateRequest = response.body();
                if (stateRequest.getStatus() == true) {
                    stateResponses = new ArrayList<StateResponse>(Arrays.asList(response.body().getData()));
                    for (int i = 0; i < stateResponses.size(); i++) {
                        State.add(stateResponses.get(i).getStateName());

//                        addresses.add(cityarray.get(i).getId());
                    }
//                    Toast.makeText(CitySreen.this, "city", Toast.LENGTH_LONG).show();
//                    cityAdapter.updateAdapter(datas, context);
//                    txtcity.setText("");

                } else {
//                    Toast.makeText(CitySreen.this, "City Not Added", Toast.LENGTH_LONG).show();


                }
            }

            @Override
            public void onFailure(Call<StateRequest> call, Throwable t) {

            }
        });
    }

    void getCityList() {


        Call<CityRequest> call = RetrofitClient.getInstance().getapi()
                .city_response(stateid);
        call.enqueue(new Callback<CityRequest>() {
            @Override
            public void onResponse(Call<CityRequest> call, Response<CityRequest> response) {

                CityRequest cityRequest = response.body();
                if (cityRequest.getStatus() == true) {



                    cityResponses = new ArrayList<CityResponse>(Arrays.asList(response.body().getCityResponses()));

                    for (int i = 0; i < cityResponses.size(); i++) {
                        City.add(cityResponses.get(i).getCityName());

//                        addresses.add(cityarray.get(i).getId());
                    }
//                    Toast.makeText(CitySreen.this, "city", Toast.LENGTH_LONG).show();
//                    cityAdapter.updateAdapter(datas, context);


                } else {
//                    Toast.makeText(CitySreen.this, "City Not Added", Toast.LENGTH_LONG).show();


                }
            }

            @Override
            public void onFailure(Call<CityRequest> call, Throwable t) {

            }
        });
    }

    void getdata() {
        strname = txtname.getText().toString();
        stremail = txtemail.getText().toString();
        strmoble = txtmobile.getText().toString();
        straddress = txtaddress.getText().toString();
        strrefer = txtrefer.getText().toString();
//        device_id = "1";

    }

    public static String getDeviceId(Context context) {
//        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
//            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
//            if (tm.getDeviceId() != null) {
//                return tm.getDeviceId();
//            } else {
//                return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
//            }
//        }
        // return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        String android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return android_id;
    }

    void createuser() {
        getdata();

        Call<Createuser> call = RetrofitClient.getInstance().getapi()
                .createuser(strname, strmoble, strrefer, stremail, getDeviceId(context), device_type, device_token, straddress, genderid, stateid, cityid);
        call.enqueue(new Callback<Createuser>() {
            @Override
            public void onResponse(Call<Createuser> call, Response<Createuser> response) {

                Createuser createuser = response.body();
                if (createuser.getStatus() == true) {
                    Preferences.setUserId(createuser.getUserId());
                    Intent intent = new Intent(SignIn.this, OtpVerification.class);
                    startActivity(intent);
//                    finish();

                } else {
                    Toast.makeText(SignIn.this, "Please Check Internet", Toast.LENGTH_LONG).show();


                }
            }

            @Override
            public void onFailure(Call<Createuser> call, Throwable t) {

            }
        });
    }

}
