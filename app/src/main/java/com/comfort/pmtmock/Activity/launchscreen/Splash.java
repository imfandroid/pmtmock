package com.comfort.pmtmock.Activity.launchscreen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.comfort.pmtmock.Activity.MainActivity;
import com.comfort.pmtmock.R;
import com.comfort.pmtmock.Services.Preferences;


public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);



        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Preferences.appContext = getApplicationContext();
        Thread th = new Thread() {
            public void run() {
                try {
                    sleep(3000);

                } catch (Exception e) {
                    e.printStackTrace();

                } finally {
                    if(Preferences.getLoginStatus())
                    {
                        Intent intent = new Intent(Splash.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }else {
                        Intent intent = new Intent(Splash.this, SliderActivity.class);
                        startActivity(intent);
                        finish();
                    }
                   /* if(Preferences.getLoginStatus())
                    {
                        Intent intent = new Intent(Splash.this, Navigation.class);

                        startActivity(intent);
                        finish();

                    }
                    else {
                        Intent intent = new Intent(Splash.this, WelcomeDemo.class);

                        startActivity(intent);
                        finish();
                    }*/
                }
            }
        };
        th.start();
    }

}

