package com.comfort.pmtmock.Activity.launchscreen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.comfort.pmtmock.Activity.MainActivity;
import com.comfort.pmtmock.Model.Response.Otp;
import com.comfort.pmtmock.R;
import com.comfort.pmtmock.Services.Preferences;
import com.comfort.pmtmock.Services.RetrofitClient;
import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpVerification extends AppCompatActivity {


    Context context;
    Button btnSubmitOtp;
    ImageButton back;


    String otpsend;
    TextView resendotp;
    String divice = "android";

    OtpView otpView;
    String strOtp;
    String userid = Preferences.getUserId();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_otp_verification);

        init();
    }


    void init() {
        context = OtpVerification.this;
        back = findViewById(R.id.back);
        backbutton();

        Preferences.appContext = OtpVerification.this;
        btnSubmitOtp = (Button) findViewById(R.id.submit);
        resendotp = findViewById(R.id.txtresend);
        otpView = findViewById(R.id.otp);
        otpView.setOtpCompletionListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String otp) {
                strOtp = otp;
            }
        });

        resed();
        resendotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resed();

            }
        });
        btnSubmitOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendotp();

            }
        });

    }
    public static String getDeviceId(Context context) {
        String android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return android_id;
    }


    void sendotp() {
        if (Preferences.getUserId() != null) {
//            String value = getIntent().getExtras().getString("userid");
            setvalue();
            Call<Otp> call = RetrofitClient.getInstance()
                    .getapi().otp_verification(userid, otpsend, divice, getDeviceId(context));
            call.enqueue(new Callback<Otp>() {
                @Override
                public void onResponse(Call<Otp> call, Response<Otp> response) {
                    Otp otp = response.body();
                    if (otp.isStatus() == true) {

//                        Preferences.setUserId(value);

                        if (otp.getMessage().equalsIgnoreCase("OTP is verified but city is not selected")) {
//                                    Preferences.setLoginStatus(true);
                            Intent intent = new Intent(OtpVerification.this, MainActivity.class);
                            startActivity(intent);

                        } else {
                            Preferences.setLoginStatus(true);
                            Intent intent = new Intent(OtpVerification.this, Login.class);
//                                    intent.putExtra("userid",value);
                            startActivity(intent);
                        }

                    } else if (otp.isStatus() == false) {

                        Toast.makeText(OtpVerification.this, otp.getMessage(), Toast.LENGTH_LONG).show();

                    }

                }

                @Override
                public void onFailure(Call<Otp> call, Throwable t) {

                }
            });


        }
    }


    void backbutton() {

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //super.onBackPressed();
                onBackPressed();
            }
        });
    }


    void setvalue() {

        otpsend = strOtp;

//        Toast.makeText(OtpVerification.this, "Message: " + otpsend, Toast.LENGTH_LONG).show();

    }

    void resed() {
        Call<Otp> call = RetrofitClient.getInstance()
                .getapi().resend_otp(Preferences.getUserId());
        call.enqueue(new Callback<Otp>() {

            @Override
            public void onResponse(Call<Otp> call, Response<Otp> response) {
                Otp otp = response.body();
                if (otp.isStatus() == true) {
                    Toast.makeText(OtpVerification.this, otp.getMessage(), Toast.LENGTH_LONG).show();

//                            sendotp();
                } else if (otp.isStatus() == false) {

                    Toast.makeText(OtpVerification.this, otp.getMessage(), Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Call<Otp> call, Throwable t) {

            }
        });

    }

}







