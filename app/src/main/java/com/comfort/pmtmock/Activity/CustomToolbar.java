package com.comfort.pmtmock.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.comfort.pmtmock.R;

public class CustomToolbar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_toolbar);
    }
}
