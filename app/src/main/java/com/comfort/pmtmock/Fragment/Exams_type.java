package com.comfort.pmtmock.Fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.comfort.pmtmock.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Exams_type extends Fragment {


    public Exams_type() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_exams_type2, container, false);
    }

}
